############################ -*- Mode: Makefile -*- ###########################
## Makefile<ftpbackup> --- 
## Author           : Carl Chenet ( chaica@ohmytux.com ) 
## Created On       : Tue Feb 23 10:00:27 2016
## Created On Node  : portable
## Last Modified By : Carl Chenet
## Last Modified On : Tue Feb 23 10:00:27 2016
## Last Machine Used: portable
## Update Count     : 1
## Status           : Unknown, Use with caution!
## HISTORY          : 
## Description      : 
## 
###############################################################################
prefix    = $(DESTDIR)

ETCDIR    = $(prefix)/etc/ftpbackup
CONF      = ftpbackup.conf.example
BINDIR    = $(prefix)/usr/bin
MANDIR    = $(prefix)/usr/share/man/
MAN1DIR   = $(MANDIR)/man1
MANFILE   = ftpbackup.1
BIN       = ftpbackup

# install commands
install_file            := /usr/bin/install -p    -o root -g root -m 644
install_private_file    := /usr/bin/install -p    -o root -g root -m 600
install_program         := /usr/bin/install -p    -o root -g root -m 755
make_directory          := /usr/bin/install -p -d -o root -g root -m 755

all build: check

check:
	bash -n ftpbackup

install:
	$(make_directory)        $(BINDIR)
	$(make_directory)        $(ETCDIR)
	$(make_directory)        $(MAN1DIR)
	$(install_program)       $(BIN)                $(BINDIR)
	$(install_private_file)  $(CONF)               $(ETCDIR)
	$(install_file)          $(MANFILE)            $(MAN1DIR)
	gzip -9fq                $(MAN1DIR)/$(MANFILE)

clean distclean:
	@echo nothing to do for clean

